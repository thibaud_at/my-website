# My Website

by Thibaud Avril-Terrones

App available on https://thibaud-avril-terrones.herokuapp.com/

Deployed with [Heroku](www.heroku.com)

## Getting started

My Website is an app built on [Angular](https://angular.io)

### Launch on local

Launch the APP : 
```bash
npm install -g @angular/cli
npm install
npm run start:dev
```

You can now try your app on http://localhost:4200

<!-- ### Launch on docker

```bash
docker build -t thibaud-avril-terrones .
docker run --name thibaud-avril-terrones -d -p 8080:4200 thibaud-avril-terrones
``` -->

You can now try your app on http://localhost

## External code for animations

- [Title](https://codepen.io/alvaromontoro/pen/RmRjvg)
- [Description](https://codepen.io/kazed972/pen/bQOQGR)
- [Education](https://michalsnik.github.io/aos/)
- [Timeline](https://codepen.io/knyttneve/pen/bgvmma/)
