FROM node:lts-alpine AS builder
WORKDIR /home/node/thibaud-avril-terrones
COPY . .
RUN npm install -g @angular/cli
RUN npm install
RUN npm cache clean --force
EXPOSE 4200
ENTRYPOINT npm run start:prod