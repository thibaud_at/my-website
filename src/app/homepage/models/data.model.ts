export interface HomepageData {
  presentation: Presentation;
  education: Array<Education>;
  experiences: Array<Experience>;
  languages: Array<Language>;
  skills: {
    hard_skills: Array<Skill>;
    soft_skills: Array<Skill>;
  };
}

export interface Presentation {
  firstname: string;
  lastname: string;
  age: number;
  title: string;
  description: string;
}

export interface Education {
  school: string;
  diploma: string;
  start_year: number;
  end_year: number;
  url: string;
  url_img: string;
}

export interface Experience {
  company: string;
  title: string;
  img: string;
  description: string;
  year: string;
}

export interface Language {
  language: string;
  url_flag: string;
  level_nb: number;
  level_string: string;
  certifications: Array<{
    name: string;
    score: number;
  }>
}

export interface Skill {
  name: string;
  level: number;
}