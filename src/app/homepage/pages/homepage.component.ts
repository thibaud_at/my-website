import { Component, OnInit, AfterViewInit } from "@angular/core";

// import { Presentation } from "../../models/presentation.model";
// import { HomepageService } from "../../services/homepage.service";

import * as data from '../data/data.json';
import { HomepageData, Presentation, Experience, Education, Language, Skill } from '../models/data.model';

declare var $: any;
// import * as $ from 'jquery';

import * as AOS from 'aos';

@Component({
  selector: "app-homepage",
  templateUrl: "./homepage.component.html",
  styleUrls: ["./homepage.component.css"],
})
export class HomepageComponent implements OnInit, AfterViewInit {
  data: HomepageData;

  constructor() {}

  ngOnInit() {
    AOS.init();

    this.data = {
      presentation: data.presentation as Presentation,
      education: data.education as Education[],
      experiences: data.experiences as Experience[],
      languages: data.languages as Language[],
      skills: {
        hard_skills : data.skills.hard_skills as Skill[],
        soft_skills : data.skills.soft_skills as Skill[],
      }   
    } as HomepageData;

    this.initializeScroll();
    this.initializeTimeline();
  }

  ngAfterViewInit() {
    this.ajustSkillsProgressBar();
  }

  initializeScroll(){
    $('.hero__scroll').on('click', function(e) {
      $('html, body').animate({
        scrollTop: $(window).height()
      }, 1200);
    });
  }

  initializeTimeline(){
    (function($) {
      $.fn.timeline = function() {
        var selectors = {
          id: $(this),
          item: $(this).find(".timeline-item"),
          activeClass: "timeline-item--active",
          img: ".timeline__img"
        };
        selectors.item.eq(0).addClass(selectors.activeClass);
        selectors.id.css(
          "background-image",
          "url(" +
            selectors.item
              .first()
              .find(selectors.img)
              .attr("src") +
            ")"
        );
        var itemLength = selectors.item.length;
        $(window).scroll(function() {
          var max, min;
          var pos = $(this).scrollTop();
          selectors.item.each(function(i) {
            min = $(this).offset().top;
            max = $(this).height() + $(this).offset().top;
            var that = $(this);
            if (i == itemLength - 2 && pos > min + $(this).height() / 2) {
              selectors.item.removeClass(selectors.activeClass);
              selectors.id.css(
                "background-image",
                "url(" +
                  selectors.item
                    .last()
                    .find(selectors.img)
                    .attr("src") +
                  ")"
              );
              selectors.item.last().addClass(selectors.activeClass);
            } else if (pos <= max - 40 && pos >= min) {
              selectors.id.css(
                "background-image",
                "url(" +
                  $(this)
                    .find(selectors.img)
                    .attr("src") +
                  ")"
              );
              selectors.item.removeClass(selectors.activeClass);
              $(this).addClass(selectors.activeClass);
            }
          });
        });
      };
    })($);
    
    $("#timeline-1").timeline();    
  }

  ajustSkillsProgressBar(){
    ["hard_skills", "soft_skills"].forEach(n => {
      for(let i=0; i<this.data.skills[n].length; i++){
        // console.log('document.getElementById(\"'+n+'_'+i+'\").style.width = \"'+this.data.skills[n][i].level+'%\";');
        document.getElementById(n+"_"+i).style.width = this.data.skills[n][i].level+'%';
      }
    });
  }
}
