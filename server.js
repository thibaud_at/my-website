const express = require('express');
const app = express();

app.use(express.static('./dist/thibaud-avril-terrones'));

app.get('/*', function(req, res) {
    res.sendFile('index.html', {root: 'dist/thibaud-avril-terrones/'});
});

const port = process.env.PORT || 8080;

app.listen(port, (err, res) => {
  if (!err) {
    console.log(`service is running on ${port}`);
  }
});